from django.views.generic import TemplateView
from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage


class IDEView(TemplateView):
    template_name = 'views/partials/ide.html'

    def get_context_data(self, **kwargs):
        context = super(IDEView, self).get_context_data(**kwargs)
        return context
