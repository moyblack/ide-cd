angular.module("IDE", ['ui.ace','djng.forms','IDE.contIde','IDE.contGlobal','IDE.services','IDE.directives','ngOnload'])
.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
})
.config(function($httpProvider){ 
    $httpProvider.defaults.headers.common['X-CSRFToken'] = 'csrf_token';
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
})
