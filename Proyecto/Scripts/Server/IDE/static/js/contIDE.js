angular.module('IDE.contIde', [])

.controller('ctrlIDE', function($scope, $http,  $interval, $timeout, GetFiles) {
  
   $scope.Acethemes = [
    "chrome",
    "clouds",
    "crimson_editor",
    "dawn",
    "dreamweaver",
    "eclipse",
    "github",
    "solarized_light",
    "textmate",
    "tomorrow",
    "xcode",
    "kuroir",
    "katzenmilch",
    "ambiance",
    "chaos",
    "clouds_midnight",
    "cobalt",
    "idle_fingers",
    "kr_theme",
    "merbivore",
    "merbivore_soft",
    "mono_industrial",
    "monokai",
    "pastel_on_dark",
    "solarized_dark",
    "terminal",
    "tomorrow_night",
    "tomorrow_night_blue",
    "tomorrow_night_bright",
    "tomorrow_night_eighties",
    "twilight",
    "vibrant_ink",
  ];
  $scope.themes = [
    {id:0, theme:"chrome"},
    {id:1, theme:"clouds"},
    {id:2, theme:"crimson_editor"},
    {id:3, theme:"dawn"},
    {id:4, theme:"dreamweaver"},
    {id:5, theme:"eclipse"},
    {id:6, theme:"github"},
    {id:7, theme:"solarized_light"},
    {id:8, theme:"textmate"},
    {id:9, theme:"tomorrow"},
    {id:10, theme:"xcode"},
    {id:11, theme:"kuroir"},
    {id:12, theme:"katzenmilch"},
    {id:13, theme:"ambiance"},
    {id:14, theme:"chaos"},
    {id:15, theme:"cobalt"},
    {id:16, theme:"idle_fingers"},
    {id:17, theme:"kr_theme"},
    {id:18, theme:"merbivore"},
    {id:19, theme:"merbivore_soft"},
    {id:20, theme:"mono_industrial"},
    {id:21, theme:"clouds_midnight"},
    {id:22, theme:"monokai"},
    {id:23, theme:"pastel_on_dark"},
    {id:24, theme:"solarized_dark"},
    {id:25, theme:"terminal"},
    {id:26, theme:"tomorrow_night"},
    {id:27, theme:"tomorrow_night_blue"},
    {id:28, theme:"tomorrow_night_bright"},
    {id:29, theme:"tomorrow_night_eighties"},
    {id:30, theme:"twilight"},
    {id:31, theme:"vibrant_ink"},
  ];
  
  var modes = [
    'abap',
    'actionscript',
    'ada',
    'apache_conf',
    'asciidoc',
    'assembly_x86',
    'autohotkey',
    'batchfile',
    'c9search',
    'c_cpp',
    'cirru',
    'clojure',
    'cobol',
    'coffee',
    'coldfusion',
    'csharp',
    'css',
    'curly',
    'd',
    'dart',
    'diff',
    'dockerfile',
    'dot',
    'dummy',
    'dummysyntax',
    'eiffel',
    'ejs',
    'elixir',
    'elm',
    'erlang',
    'forth',
    'ftl',
    'gcode',
    'gherkin',
    'gitignore',
    'glsl',
    'golang',
    'groovy',
    'haml',
    'handlebars',
    'haskell',
    'haxe',
    'html',
    'html_ruby',
    'ini',
    'io',
    'jack',
    'jade',
    'java',
    'javascript',
    'json',
    'jsoniq',
    'jsp',
    'jsx',
    'julia',
    'latex',
    'less',
    'liquid',
    'lisp',
    'livescript',
    'logiql',
    'lsl',
    'lua',
    'luapage',
    'lucene',
    'makefile',
    'markdown',
    'matlab',
    'mel',
    'mushcode',
    'mysql',
    'nix',
    'objectivec',
    'ocaml',
    'pascal',
    'perl',
    'pgsql',
    'php',
    'powershell',
    'praat',
    'prolog',
    'properties',
    'protobuf',
    'python',
    'r',
    'rdoc',
    'rhtml',
    'ruby',
    'rust',
    'sass',
    'scad',
    'scala',
    'scheme',
    'scss',
    'sh',
    'sjs',
    'smarty',
    'snippets',
    'soy_template',
    'space',
    'sql',
    'stylus',
    'svg',
    'tcl',
    'tex',
    'text',
    'textile',
    'toml',
    'twig',
    'typescript',
    'vala',
    'vbscript',
    'velocity',
    'verilog',
    'vhdl',
    'xml',
    'xquery',
    'yaml',
];
  
    $scope.tokenInformation = "";
    $scope.currentTheme = 14;
    $scope.exp = 1;
    $scope.gpo = 1234
    $scope.tarea = 37;

  $scope.aceLoaded = function (_editor) {
     var _session = _editor.getSession();
    var _renderer = _editor.renderer;

    // Options
     _editor.$blockScrolling = Infinity;
    _editor.setShowPrintMargin(false);

    // Events
     _editor.on('mousedown', function(e) {
    var row = e.getDocumentPosition().row;
    var col = e.getDocumentPosition().column;
    var token = _editor.session.getTokenAt(row, col);
    $scope.STInfo(String(token.type));
    });

    _editor.on('change', function () {
       $scope.uptdateCodigo(String(_editor.session.getValue()));
   });
  }
  
  $scope.aceChanged = function (_editor) {

  }

  var editor = function() {
    this.theme = $scope.Acethemes[$scope.currentTheme];
    this.mode = 'python';
    this.opacity = 100;
    this.useWrapMode = true;
    this.gutter = true;
    this.splitMode = false;
    this.fontS =16;
    
    this.snippets = true;
    this.bAuto = false;
    this.lAuto = true;
    this.minL = Infinity;
    this.hihlightAL = true;
    this.behaveE = true;
    $scope.showToken = true;

  };
  $scope.editor = new editor();


  $scope.actTheme = function()
  {
    $scope.editor.theme = $scope.Acethemes[$scope.selectedTheme.id];
  }

  $scope.split = function()
  {
    if ($scope.editor.splitMode)
    {
        $scope.editor.splitMode = false;
    }
    else
    {
        $scope.editor.splitMode = true;
    }
    
  }

  $scope.letraG = function()
  {
    $scope.editor.fontS = 24;
  }

  $scope.letraN = function()
  {
    $scope.editor.fontS = 16;
  }

  $scope.letraC =  function()
  {
    $scope.editor.fontS = 10;
  }
   $scope.STInfo = function(tokenInfo)
   {
     $scope.$apply(function () {
            $scope.tokenInformation = tokenInfo;
        });  
   }

  $scope.saveCode = function()  
  {
    var code = $scope.codigo;
    alert(code);
    var variablesToSend = {c: $scope.codigo};
    $http.post('http://localhost:8000/api/savecode/'+$scope.exp+'/'+$scope.gpo+'/'+$scope.tarea+'/'+ String($scope.files[$scope.tab - 1]), variablesToSend ).then(function(response){
       console.log(response);
      sweetAlert("Exito", "Codigo Guardado", "success");
   }, function(response){
      console.log(response);
      sweetAlert("Error", "Ocurrio un problema", "error");
   });
    }
    $scope.init = function()
    {  
        $scope.getFiles();
        $scope.blockedit =true;
        $scope.puedeeditar = true;
        $scope.shwAdmin = false;
    }
    $scope.getFiles = function()
    {
        var recieved_data = GetFiles.obtainfiles($scope.exp,$scope.gpo, $scope.tarea);

        recieved_data.then(
            function(fulfillment){
                if(String(fulfillment) != "Ocurrio un problema")
                        {
                            var files = fulfillment;
                            $scope.files = files.split(",");
                            $scope.numFiles = $scope.files.length;
                            $scope.tab=1;
                            $scope.getCodes();
                        }
                        else
                        {
                            sweetAlert("Aviso", String(fulfillment), "error");
                        }
            }, function(){
                 sweetAlert("Error", "Ocurrio un problema", "error");
        });
    }
    $scope.getCodes = function()
    {
        var recieved_code = GetFiles.obtaincode($scope.exp,$scope.gpo, $scope.tarea, String($scope.files[$scope.tab - 1]));
                recieved_code.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Ocurrio un problema")
                        {
                            $scope.codigo = fulfillment;
                        }
                        else
                        {
                            sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        
                    });
    }
    $scope.changeTab = function (value)
    {
        $scope.tab = value;
        var recieved_code = GetFiles.obtaincode($scope.exp,$scope.gpo, $scope.tarea, String($scope.files[$scope.tab - 1]));
                recieved_code.then(
                    function(fulfillment){
                            $scope.codigo = fulfillment;
                    });
    }
    $scope.uptdateCodigo = function (code)
    {
        $scope.codigo = code;
    }
    $scope.nuevoArchivo = function(filename)
    {
        var recieved_data = GetFiles.createfile($scope.exp,$scope.gpo, $scope.tarea, filename);
                recieved_data.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Archivo Creado")
                        {
                           sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        else
                        {
                            $scope.getFiles();
                        }
                        
                    });

        $scope.newFileName ='';
    }
    $scope.eliminarArchivo = function(filename)
    {
        var recieved_data = GetFiles.deletefile($scope.exp,$scope.gpo, $scope.tarea, filename);
                recieved_data.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Archivo Borrado")
                        {
                           sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        else
                        {
                            $scope.getFiles();
                        }
                        
                    });
    }
    $scope.editarNombre = function(fileName)
    {
        $scope.tempFilename = fileName;
               alert($scope.tempFilename);
    }
    $scope.guardarNombre = function(fileName)
    {
       var recieved_data = GetFiles.renamefile($scope.exp,$scope.gpo, $scope.tarea,  $scope.tempFilename,fileName);

                recieved_data.then(
                    function(fulfillment){
                        if(String(fulfillment) != "Nombre de Archivo Cambiado")
                        {
                           sweetAlert("Aviso", String(fulfillment), "error");
                        }
                        else
                        {
                            $scope.getFiles();
                        }
                        
                    });
        $scope.tempFilename ='';
    }
    $scope.showAdmin = function()
    {
        $scope.shwAdmin = true;
    }
    $scope.SalirAdminProyecto = function()
    {
        $scope.shwAdmin = false;
    }
});