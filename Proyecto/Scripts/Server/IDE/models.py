from __future__ import unicode_literals
from django.db import models
import datetime

class Sistema_Catalogo_Metricas(models.Model):
    categoria = models.CharField(max_length=100)
    nombre_metrica = models.CharField(max_length=100)
    descripcion_metrica = models.CharField(max_length=1000)
    tieneIndicadores = models.BooleanField(default = False)
    indicadorAlto = models.IntegerField( default=0)
    indicadorBajo = models.IntegerField(default =0)

class Metricas(models.Model):
    expediente = models.CharField(max_length=100, default='0')
    grupo = models.CharField(max_length=100, default='0')
    ideTarea = models.CharField(max_length=100, default='0')
    id_metrica = models.ForeignKey(Sistema_Catalogo_Metricas, on_delete=models.CASCADE,default=0 )
    calificacion = models.IntegerField(default=0)
    registro = models.DateTimeField(auto_now_add=True)
