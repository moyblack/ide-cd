from django.conf.urls import url
from .views import IDEView

urlpatterns = [
    url(r'^main$', IDEView.as_view(), name='ide'),
]
