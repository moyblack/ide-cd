# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-06-28 15:03
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Catalogo_Metricas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('categoria', models.CharField(max_length=100)),
                ('nombre_metrica', models.CharField(max_length=100)),
                ('descripcion_metrica', models.CharField(max_length=1000)),
                ('tieneIndicadores', models.BooleanField(default=False)),
                ('indicadorAlto', models.IntegerField(default=0)),
                ('indicadorBajo', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Metricas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('expediente', models.CharField(default='0', max_length=100)),
                ('grupo', models.CharField(default='0', max_length=100)),
                ('ideTarea', models.CharField(default='0', max_length=100)),
                ('calificacion', models.IntegerField(default=0)),
                ('registro', models.DateTimeField(auto_now_add=True)),
                ('id_metrica', models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='IDE.Catalogo_Metricas')),
            ],
        ),
    ]
