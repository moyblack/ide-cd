from __future__ import unicode_literals
from django.db import models
from datetime import datetime
from django.utils import timezone


class Proyectos(models.Model):
    expediente = models.CharField(max_length=100, default='0')
    grupo = models.CharField(max_length=100, default='0')
    ideTarea = models.CharField(max_length=100, default='0')
    path = models.CharField(max_length=200, default='/')
    fecha = models.DateTimeField(auto_now_add=True)
    status = models.SmallIntegerField(default=0)

    def __unicode__(self):
        return self.ideTarea or 'noname'