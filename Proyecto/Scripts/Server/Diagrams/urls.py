from django.conf.urls import url
from .views import getCode,prueba
urlpatterns = [
    url(r'^getnotify/', prueba, name='notificateGetCode'),
    url(r'^getnotification/(?P<exp>[0-9]+)/(?P<gpo>[0-9]+)/(?P<tarea>[0-9]+)/(?P<fileName>[\w\-]+)$', getCode, name='notificateGetCode'),
]
